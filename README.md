Zombie Infection Simulation for Australian and New Zealand Cities
===============

### Credits ###

The zombie infection model is derived from the SZR (Susceptible-Zombie-Removed) model proposed and implemented by Alemi et al. (2015). Original source code is 
forked from (https://github.com/mattbierbaum/zombies-usa). 


### Online Demo ###

http://apps.csdila.ie.unimelb.edu.au/zombies/


### Data Preparation ###

Population data are required for the zombie infection simulation. For three capital cities in Australia including Melbourne, Sydney and Perth, the latest 2016 Census (ABS 2016) population data at Meshblock level is adopted. For the Auckland and Queenstown regions in New Zealand, the 2013 Census (Statistics New Zealand 2013) population data at Meshblock level is adopted. Meshblock is the smallest geographical unit for which Census data are available; hence, the population at Meshblock level is the most accurate data that are publicly accessible for these two countries.

The raw population data needs to be converted into 1kmX1km population grid format before the zombie infection model can be applied on it. The following spatial analysis and process steps are performed in QGIS software:

Step 1: reproject Meshblock geometry from the original geographical coordinate system (longitude and latitude, unit is arc degree) to a proper projected coordinate system (x and y, unit is meter);

Step 2: for each Meshblock region, use the "Random points" process to generate a number of random points within that region based its population number;

Step 3: for each city, use the "ector grid" tool to create grid polygons at the resolution of 0.5km X 0.5km;

Step 4: feed the outputs of Step 2 and 3 to the "Count points in polygon" tool to calculate the population number in each grid;

Step 5: use the "Rasterize" process to convert the vector population grid into a raster image;

Step 6: run "gen_jsondata_from_raster.r" script to perform morphological "Closing" operation (mmand 2017) on the raster image to increase the connectivity of population grids, and then export the population grid data in a 2D array format for the simulation tool.


### Contact ###

Dr Yiqun Chen

Centre for Disaster Management & Public Safety

Centre for Spatial Data Infrastructures & Land Administration

The University of Melbourne

E: yiqun.c@unimelb.edu.au