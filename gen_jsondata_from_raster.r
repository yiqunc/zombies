library(raster)
library(RJSONIO)
library(mmand)
setwd("C:\\Users\\chen1\\SourceCodeRepos\\zombies")

# tif image can be scaled up and down to fit the webpage, while after scaling, population values should be changed accordingly.
# for example, if the popgrid is orginally created at 1km resolution, while the saved tif is at 0.5km resulation, then  regionScaleFactor = 2;
# for another example, if the popgrid is orginally created at 1km resolution, while the saved tif is at 2km resulation, then  regionScaleFactor = 0.5;
# for one more example, if the popgrid is orginally created at 1km resolution, while the saved tif is at 1km resulation, then  regionScaleFactor = 1;

regionScaleFactor = 0.5 

regionName = 'nz_2km'

str_name<-sprintf('dat\\%s.tif', regionName)
dat=raster(str_name)
arr = as.matrix(dat)

# recalculate the population
arr = arr / (regionScaleFactor*regionScaleFactor)

# in the raster, the origin starts from upper-left corner 
# while the json data should treat the lower-left cornor as origin 
# so that it can match with mouse click coords on the map, 
# we need to reverse row of the matrix for this purpose 
# ref: https://stackoverflow.com/questions/36914333/how-to-reverse-rows-in-matrix
rev_arr = arr[nrow(arr):1, ]

# convert to integer 
# rev_arr[] = as.integer(rev_arr)

#create a binary array as a template for morphological operation
binary_arr = ifelse(rev_arr>0, 1, 0)
# image(binary_arr, useRaster=TRUE, axes=FALSE)

# define a kernal, bigger size, better connected (i.e., less holes)
kn = shapeKernel(c(9,9), type="diamond")

# image(binary_arr, useRaster=TRUE, axes=FALSE)
# try to fill holes in the population grid gap using closing method, 
# run multiple times and the binary array will eventually be stable
binary_arr = closing(binary_arr, kn)

# add the filled holes back to the population array
rev_arr = ifelse(rev_arr>0, rev_arr, rev_arr+binary_arr)

# remove all whitespace before saving
write(gsub(" ", "", toJSON(rev_arr)), file=sprintf('dat\\%s.json', regionName))

